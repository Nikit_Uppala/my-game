Can You Cross the Road?



Overview - The game arena consists of a road which has many lanes. There is a player (player 1 and player 2 one at a time) who is present at one end of the road. The player wants to go to the other end as fast as possible. But in some of the lanes, there are manholes because of which the player is afraid that he may fall into it and also some lanes have cars moving from right to left. So, it is your duty to help him cross the road.



Scoring Scheme - 
If the player moves from a lane containing a car to the next lane, then the player gets 10 points.
If the player moves from a lane containing a man-hole to the next lane, then player gets 5 points.
For every 2 seconds 1 point is deducted for the player.



Controls -
Player 1 
-> up arrow - move up
-> down arrow - move down
-> left arrow - move left
-> right arrow - move right

Player 2
-> w - move up
-> s - move down
-> a - move left
-> d - move right


Game Over Criteria - 
Game continues till both the players collide with an obstacle.(Player 1 will advance even if player 2 collided and vice-versa).

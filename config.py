# Colors for players given in rgb format (red, green, blue)
player1_foreground = (255, 255, 255)
player2_foreground = (0, 0, 0)
player1_background = (0, 0, 0)
player2_background = (255, 255, 255)
font = "Arial"
hit_image = r"./images/hit_message.png"


def get_player1_color():
    return list([player1_foreground, player1_background])


def get_player2_color():
    return list([player2_foreground, player2_background])


def get_font():
    return font


def get_image_hit():
    return hit_image


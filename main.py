import pygame
import random
import config


class MovingObstacle:
    def display(self, x, y):
        window.blit(images["car"], (x, y))


class FixedObstacle:
    def display(self, x, y):
        window.blit(images["manhole"], (x, y))


time = 0
pygame.init()
window_size = (964, 957)
window = pygame.display.set_mode(window_size)
pygame.display.set_caption("Can you get across?")
fixed_obstacles_segments = [0, window_size[0] // 3, 2 * window_size[0] // 3]
fixed_obstacles_segments.append(window_size[0])
player_start_positions = [[window_size[0] / 2, window_size[1] - 45],
                          [window_size[0] / 2, 33]]

fixed_obstacles = list([])
for i in range(4):
    fixed_obstacles.append([])
    for j in range(3):
        if random.choice([0, 1]) == 1:
            fixed_obstacles[i].append(random.randint(
                fixed_obstacles_segments[j],
                fixed_obstacles_segments[j+1] - 135))
        if j == 2 and len(fixed_obstacles[i]) == 0:
            fixed_obstacles[i].append(random.randint(
                fixed_obstacles_segments[j],
                fixed_obstacles_segments[j+1] - 135))

game_running = True
display_home = True
game_over = list([False, False])
score = [0, 0]

max_fps = 30
road = tuple((80, 80, 80))
black = tuple((0, 0, 0))
white = (255, 255, 255)
score_background = (255, 153, 0)
clock = pygame.time.Clock()
velocity_of_car = [7, 7]
level_of_players = [1, 1]
current_player = 0   # 0 means player1 and 1 means player2
images = {}
images["car"] = pygame.image.load("./images/car.png")
images["line_image"] = pygame.image.load("./images/Feb-web-yellow-line.png")
images["manhole"] = pygame.image.load("./images/manhole.png")
images["manhole"] = pygame.transform.scale(images["manhole"], (110, 110))
line_gap = window_size[1] / 11
# 0 has foreground and 1 has background.
player_colors = [config.get_player1_color(), config.get_player2_color()]
car = MovingObstacle()
fixed_obstacle = FixedObstacle()
position = window_size[1] / 11
font = pygame.font.SysFont(config.get_font(), 30)
maximum_segment = 0
number_of_segments = 11
segments_y = []
positionx = []
userText = f"Player {current_player+1}"
userSymbol = font.render(userText, True,
                         player_colors[0][0], player_colors[0][1])
user_x = [window_size[0] / 2, window_size[0] / 2]
user_y = [window_size[1] - 45, 25]
segments_user = 0
car_width = 150
segments_y = []


def check_winner():
    if level_of_players[0] > level_of_players[1]:
        return "Player 1 Wins!"
    elif level_of_players[0] < level_of_players[1]:
        return "Player 2 Wins!"
    if score[0] > score[1]:
        return "Player 1 Wins!"
    elif score[0] < score[1]:
        return "Player 2 Wins!"
    else:
        return "Game Tied"


def game_over_screen():
    global game_over, time
    player1_score = (window_size[0] / 3 - 190, window_size[1] / 3)
    player2_score = (window_size[0] / 3 - 190, window_size[1] / 3 + 100)
    window.fill(black)
    display_text("Result", white, window_size[0] / 2 - 100,
                 window_size[1] / 4 - 80, 75)
    player1 = f"Player 1 Level and Score: {level_of_players[0]}   {score[0]}"
    display_text(player1, white, player1_score[0], player1_score[1], 65)
    player2 = f"Player 2 Level and Score: {level_of_players[1]}   {score[1]}"
    display_text(player2, white, player2_score[0], player2_score[1], 65)
    game_result = check_winner()
    display_text(game_result, white, player1_score[0],
                 window_size[1] / 1.8, 70)
    display_text("Press ENTER to play again", white, player2_score[0],
                 window_size[1] / 1.4, 65)
    time = pygame.time.get_ticks() // 2000


def display_line(x, y):
    window.blit(images["line_image"], (x, y))


def display_text(text, color, x, y, font_size, background=None):
    fontObject = pygame.font.SysFont(config.get_font(), font_size - 18)
    content = fontObject.render(text, True, color, background)
    window.blit(content, (x, y))


def change_player():
    global maximum_segment, current_player, user_x, user_y
    global time, score, userSymbol, segments_user
    user_y[current_player] = player_start_positions[current_player][1]
    user_x[current_player] = player_start_positions[current_player][0]
    current_player = (current_player+1) % 2
    user_y[current_player] = player_start_positions[current_player][1]
    user_x[current_player] = player_start_positions[current_player][0]
    userText = f"Player {current_player+1}"
    foreground = player_colors[current_player][0]
    background = player_colors[current_player][1]
    userSymbol = font.render(userText, True, foreground, background)
    if current_player == 0:
        segments_user = 0
        maximum_segment = 0
    elif current_player == 1:
        maximum_segment = number_of_segments - 1
        segments_user = number_of_segments - 1
    time = pygame.time.get_ticks() // 2000
    pygame.time.delay(300)

hit_image = pygame.image.load(config.get_image_hit())
hit_image = pygame.transform.scale(hit_image, (110, 150))


def check_collision(segment_number):
    global game_over, current_player, fixed_obstacles
    distance = abs(user_x[current_player] -
                   positionx[(number_of_segments-segment_number) // 2 - 1])
    if segment_number == 0 or segment_number == number_of_segments - 1:
        return
    if segment_number & 1:
        if distance < 134:
            game_over[current_player] = True
            pygame.time.delay(50)
            window.blit(hit_image, (user_x[current_player],
                        user_y[current_player] - 80))
            pygame.display.update()
            pygame.time.delay(300)
            time = pygame.time.get_ticks()
            if not game_over[(current_player + 1) % 2]:
                change_player()
            return
    if not(segment_number & 1):
        fixed_obstacle_segment_index = (number_of_segments-segment_number)
        fixed_obstacle_segment_index //= 2
        fixed_obstacle_segment_index -= 1
        length = len(fixed_obstacles[fixed_obstacle_segment_index])
        for i in range(length):
            if abs(fixed_obstacles[fixed_obstacle_segment_index][i] -
                   user_x[current_player]) < 103:
                game_over[current_player] = True
                pygame.time.delay(50)
                window.blit(hit_image, (user_x[current_player],
                            user_y[current_player] - 80))
                pygame.display.update()
                pygame.time.delay(300)
                time = pygame.time.get_ticks()
                if not game_over[(current_player + 1) % 2]:
                    change_player()
                return


for i in range(number_of_segments // 2):
    positionx.append(random.randint(0, window_size[0]))
start_end = ("Start", "End")


def reinitialise():
    global current_player, game_over, user_x, user_y, segments_user, score
    global current_player, velocity_of_car, userSymbol
    global positionx, number_of_segments, maximum_segment
    global level_of_players, fixed_obstacles, fixed_obstacles_segments
    maximum_segment = 0
    current_player = 0
    velocity_of_car = [7, 7]
    current_player = 0
    game_over[0] = False
    game_over[1] = False
    user_x = [window_size[0] / 2, window_size[0] / 2]
    user_y = [window_size[1] - 45, 45]
    segments_user = 0
    positionx.clear()
    for i in range(5):
        positionx.append(random.randint(0, window_size[0] - 125))
    fixed_obstacles.clear()
    start = 0
    end = 0
    for i in range(4):
        fixed_obstacles.append([])
        for j in range(3):
            if random.choice([0, 1]) == 1:
                start = fixed_obstacles_segments[j]
                end = fixed_obstacles_segments[j + 1]
                fixed_obstacles[i].append(random.randint(start, end - 110))
            elif j == 2 and len(fixed_obstacles[i]) == 0:
                start = fixed_obstacles_segments[j]
                end = fixed_obstacles_segments[j + 1]
                fixed_obstacles[i].append(random.randint(start, end - 110))
    score = [0, 0]
    foreground = player_colors[0][0]
    background = player_colors[0][1]
    userSymbol = font.render(f"Player {current_player + 1}", True,
                             foreground, background)
    level_of_players[0] = 1
    level_of_players[1] = 1


def display_home_screen():
    global time
    starting_y_coordinate = 40
    window.fill(black)
    heading = "Can you get across?"
    display_text(heading, white, window_size[0] / 2 - 270,
                 starting_y_coordinate, 75)
    instructions = list(["Instructions:",
                        "1. You should go from start to end."])
    instructions.append("2. Cars are moving obstacles. ")
    instructions[len(instructions) - 1] += "Don't come in their way."
    instructions.append("3. Man-holes are fixed obstacles. ")
    instructions[len(instructions) - 1] += "Don't fall in them."
    instructions.append("4. If you cross a car you get 10 points.")
    instructions.append("5. If you cross a manhole you get 5 points.")
    instructions.append("6. For every 2 seconds your ")
    instructions[len(instructions) - 1] += "score gets reduced by 1 points."
    instructions.append("Press ENTER to start the game")
    starting_y_coordinate = starting_y_coordinate + 80
    display_text("Made By Nikit", white, window_size[0] / 2 - 120,
                 starting_y_coordinate, 40)
    starting_y_coordinate += 100
    for line in instructions:
        starting_y_coordinate = starting_y_coordinate + 65
        display_text(line, white, 20, starting_y_coordinate, 45)
    time = pygame.time.get_ticks() // 2000


def reduce_score_based_ontime(player):
    global score, time
    if time < pygame.time.get_ticks() // 2000:
        score[player] -= 1
        time = pygame.time.get_ticks() // 2000


while game_running:
    position = window_size[1]/11
    positiony = position + 15

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_running = False
        if event.type == pygame.KEYDOWN:
            if not game_over[0]:
                if current_player == 0:
                    if event.key == pygame.K_RIGHT:
                        user_x[current_player] = (user_x[current_player]+50)
                        user_x[current_player] %= window_size[0]
                    elif event.key == pygame.K_LEFT:
                        user_x[current_player] = (user_x[current_player]-50)
                        user_x[current_player] %= window_size[0]
                    elif event.key == pygame.K_UP:
                        if segments_user < number_of_segments-1:
                            user_y[current_player] -= 88
                        if current_player == 0:
                            if segments_user > maximum_segment:
                                maximum_segment = segments_user
                                if segments_user and not segments_user & 1:
                                    score[current_player] += 5
                                elif segments_user % 2 == 1:
                                    score[current_player] += 10
                            segments_user += 1

                    elif event.key == pygame.K_DOWN:
                        if segments_user > 0:
                            user_y[current_player] += 88
                            segments_user -= 1
            if not game_over[1]:
                if current_player == 1:
                    if event.key == pygame.K_d:
                        user_x[current_player] = (user_x[current_player]+50)
                        user_x[current_player] %= window_size[0]
                    elif event.key == pygame.K_a:
                        user_x[current_player] = (user_x[current_player]-50)
                        user_x[current_player] %= window_size[0]
                    elif event.key == pygame.K_w:
                        if segments_user < number_of_segments-1:
                            user_y[current_player] -= 88
                            segments_user += 1
                    elif event.key == pygame.K_s:
                        if segments_user > 0:
                            user_y[current_player] += 88
                            if segments_user < maximum_segment:
                                maximum_segment = segments_user
                                if current_player == 1:
                                    if segments_user != number_of_segments - 1:
                                        if segments_user % 2 == 0:
                                            score[current_player] += 5
                                    if segments_user != number_of_segments - 1:
                                        if segments_user % 2 == 1:
                                            score[current_player] += 10
                            segments_user -= 1
            if event.key == pygame.K_RETURN:
                if (game_over[0] and game_over[1]) or display_home:
                    display_home = False
                    reinitialise()
                    continue
    if display_home:
        display_home_screen()
        pygame.display.update()
        continue
    if game_over[0] and game_over[1]:
        game_over_screen()
        pygame.display.update()
        continue

    window.fill(road)
    numOfCars = len(positionx)
    for i in range(number_of_segments):
        display_line(0, position)
        if len(segments_y) <= number_of_segments:
            segments_y.append(position-13)
        position = position+line_gap

    for i in range(numOfCars):
        car.display(positionx[i], positiony)
        positiony = positiony + line_gap * 2
        if len(segments_y) != numOfCars:
            segments_y.append(positiony)
    for i in range(numOfCars):
        positionx[i] = (positionx[i]+velocity_of_car[current_player])
        positionx[i] %= window_size[0]
    for i in range(number_of_segments // 2 - 1):
        number_of_obstacles = len(fixed_obstacles[i])
        for j in range(number_of_obstacles):
            fixed_obstacle.display(fixed_obstacles[i][j],
                                   segments_y[(2 * (i + 1) - 1)])
    display_text(start_end[current_player], white,
                 window_size[0] / 2 - 45, window_size[1] - 45, 45)
    display_text(start_end[(current_player + 1) % 2],
                 white, window_size[0] / 2 - 40, 19, 45)
    display_text("Player 1 Score: " + str(score[0]), black, 0, 0, 45,
                 score_background)
    display_text("Player 2 Score: " + str(score[1]), black, 0, 25, 45,
                 score_background)
    window.blit(userSymbol, (user_x[current_player], user_y[current_player]))
    display_text(f"Level {level_of_players[current_player]}", white,
                 window_size[0] - 130, 0, 40)
    pygame.display.update()
    if segments_user == number_of_segments - 1 and current_player == 0:
        level_of_players[current_player] += 1
        velocity_of_car[current_player] += 3
        if not game_over[(current_player + 1) % 2]:
            change_player()
        else:
            user_x[current_player] = player_start_positions[current_player][0]
            user_y[current_player] = player_start_positions[current_player][1]
            segments_user = 0 if not current_player else number_of_segments-1
            maximum_segment = segments_user
    if segments_user == 0 and current_player == 1:
        level_of_players[current_player] += 1
        velocity_of_car[current_player] += 3
        if not game_over[(current_player + 1) % 2]:
            change_player()
        else:
            user_x[current_player] = player_start_positions[current_player][0]
            user_y[current_player] = player_start_positions[current_player][1]
            segments_user = 0 if not current_player else number_of_segments - 1
            maximum_segment = segments_user

    check_collision(segments_user)
    clock.tick(max_fps)
    reduce_score_based_ontime(current_player)

pygame.quit()
quit()
